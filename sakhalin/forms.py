from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    PasswordField,
    SubmitField,
    TextAreaField,
    FileField,
    DecimalField,
    SelectField
)
from wtforms.validators import DataRequired, Email, Length, Optional


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign In')


class ItemForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    description = TextAreaField(
        'Description',
        validators=[Length(min=0, max=140)]
    )
    price = DecimalField('Price', validators=[DataRequired()])
    status = SelectField(
        'Status',
        choices=[
            ('none', 'None'),
            ('sold_out', 'Sold out'),
            ('special', 'Special'),
            ('new', 'New')
        ]
    )
    image = FileField('Image')
    category_id = SelectField('Category', coerce=int, validators=[Optional()])
    submit = SubmitField('Submit')


class CategoryForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    submit = SubmitField('Submit')


class UserForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Submit')
