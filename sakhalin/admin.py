from flask import (
    Blueprint,
    render_template,
    current_app as app,
    abort,
    flash,
    redirect,
    url_for,
    request
)
from flask_login import login_required, current_user
from werkzeug.urls import url_parse

from . import db
from .models import Item, Category, User
from .forms import ItemForm, CategoryForm, UserForm
from .utils import image_to_base64

bp = Blueprint('admin', __name__, url_prefix='/admin')


def check_admin():
    if current_user.email not in app.config['ADMIN']:
        abort(403)


# items management
@bp.route('/items', defaults={'category_id': None})
@bp.route('/items/<int:category_id>')
@login_required
def show_item_list(category_id):
    check_admin()

    items = Item.query.filter_by(category_id=category_id) if category_id else Item.query
    items = items.outerjoin(Category).order_by(Category.name).all()
    categories = Category.query.order_by(Category.name).all()
    current_category = Category.query.get(category_id) if category_id else None

    if category_id and not Category.query.get(category_id):
        abort(404)

    return render_template(
        'admin/rd_item.html',
        title='Item list',
        items=items,
        categories=categories,
        current_category=current_category,
        # next=request.path
    )


def item_list_next_page():
    next_page = request.args.get('next')

    if not next_page or url_parse(next_page).netloc != '':
        next_page = url_for('admin.show_item_list')

    return next_page


@bp.route('/items/create', methods=('GET', 'POST'))
@login_required
def create_item():
    check_admin()

    form = ItemForm()
    form.category_id.choices = [(c.id, c.name)
                                for c in Category.query.order_by('name')]
    form.category_id.choices.insert(0, (0, 'No category'))

    if form.validate_on_submit():
        item = Item(
            name=form.name.data,
            description=form.description.data,
            price=form.price.data,
            status=form.status.data,
            category_id=form.category_id.data
        )
        if form.image.data:
            item.image = image_to_base64(form.image.data, (512, 512))
        db.session.add(item)
        db.session.commit()
        flash('Item added')

        return redirect(item_list_next_page())

    return render_template(
        'admin/cud_item.html',
        title='Create Item',
        form=form
    )


@bp.route('/items/update/<int:item_id>', methods=('GET', 'POST'))
@login_required
def update_item(item_id):
    check_admin()

    item = Item.query.get(item_id)
    if item is None:
        flash('No item with this id')
        return redirect(url_for('admin.show_item_list'))
    form = ItemForm()
    form.category_id.choices = [(c.id, c.name)
                                for c in Category.query.order_by('name')]
    form.category_id.choices.insert(0, (0, 'No category'))
    if form.validate_on_submit():
        item.name = form.name.data
        item.description = form.description.data
        item.price = form.price.data
        item.status = form.status.data
        item.category_id = form.category_id.data
        if form.image.data:
            item.image = image_to_base64(form.image.data, (512, 512))
        db.session.commit()
        flash('Item edited')

        return redirect(item_list_next_page())

    if request.method == 'GET':
        form.name.data = item.name
        form.description.data = item.description
        form.price.data = item.price
        form.status.data = item.status
        form.category_id.data = item.category_id
    return render_template(
        'admin/cud_item.html',
        title=item.name,
        form=form,
        id=item.id,
        image=item.image
    )


@bp.route('/items/delete/<int:item_id>')
@login_required
def delete_item(item_id):
    check_admin()

    item = Item.query.get(item_id)
    if item is None:
        flash('No item with this id')

        return redirect(url_for('admin.show_item_list'))

    db.session.delete(item)
    db.session.commit()
    flash('Item deleted')

    return redirect(item_list_next_page())


@bp.route('/items/delete', methods=('POST',))
@login_required
def delete_items():
    check_admin()

    for item_id in request.form:
        item = Item.query.get(item_id)
        if not item:
            continue
        db.session.delete(item)
    db.session.commit()
    flash('Items deleted')

    return redirect(item_list_next_page())


# categories management
@bp.route('/categories')
@login_required
def show_category_list():
    check_admin()

    categories = Category.query.order_by('name').all()
    return render_template(
        'admin/rd_category.html',
        title='Categories',
        categories=categories
    )


@bp.route('/categories/create', methods=('GET', 'POST'))
@login_required
def create_category():
    check_admin()

    form = CategoryForm()
    if form.validate_on_submit():
        category = Category.query.filter_by(name=form.name.data).first()
        if category is not None:
            flash('Please use a different category name')
            return render_template(
                'admin/cud_category.html',
                title='Create Category',
                form=form
            )
        category = Category(name=form.name.data)
        db.session.add(category)
        db.session.commit()
        flash('Category added')

        return redirect(url_for('admin.show_category_list'))

    return render_template(
        'admin/cud_category.html',
        title='Create Category',
        form=form
    )


@bp.route('/categories/update/<int:category_id>', methods=('GET', 'POST'))
@login_required
def update_category(category_id):
    check_admin()

    category = Category.query.get(category_id)
    if category is None:
        flash('No category with this id')

        return redirect(url_for('admin.show_category_list'))

    form = CategoryForm()
    if form.validate_on_submit():
        if form.name.data:
            category2 = Category.query.filter_by(name=form.name.data).first()
            if category2 is not None and category2.id != category.id:
                flash('Please use a different category name')

                return render_template(
                    'admin/cud_category.html',
                    title=category.name,
                    form=form,
                    id=category.id,
                )

            category.name = form.name.data
        db.session.commit()
        flash('Category edited')

        return redirect(url_for('admin.show_category_list'))

    if request.method == 'GET':
        form.name.data = category.name

    return render_template(
        'admin/cud_category.html',
        title=category.name,
        form=form,
        id=category.id
    )


@bp.route('/categories/delete/<int:category_id>')
@login_required
def delete_category(category_id):
    check_admin()

    category = Category.query.get(category_id)
    if category is None:
        flash('No category with this id')

        return redirect(url_for('admin.show_category_list'))

    db.session.delete(category)
    db.session.commit()
    flash('Category deleted')

    return redirect(url_for('admin.show_category_list'))


@bp.route('/categories/delete', methods=('POST',))
@login_required
def delete_categories():
    check_admin()

    for category_id in request.form:
        db.session.delete(Category.query.get(category_id))
    db.session.commit()

    return redirect(url_for('admin.show_category_list'))


# users management
@bp.route('/users', methods=('GET', 'POST'))
@login_required
def show_user_list():
    check_admin()

    users = User.query.all()
    form = UserForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None:
            flash('Please use a different email')
            return render_template(
                'admin/crud_user.html',
                title='User list',
                users=users,
                form=form
            )
        user = User(email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        users.append(user)
        flash('User added')
    return render_template(
        'admin/crud_user.html',
        title='User list',
        users=users,
        form=form
    )


@bp.route('/users/delete/<int:user_id>')
@login_required
def delete_user(user_id):
    check_admin()

    user = User.query.get(user_id)
    if user is None:
        flash('No user with this id')
        return redirect(url_for('admin.show_user_list'))
    db.session.delete(user)
    db.session.commit()
    flash('User deleted')
    return redirect(url_for('admin.show_user_list'))


@bp.route('/users/delete', methods=('POST',))
@login_required
def delete_users():
    check_admin()

    for user_id in request.form:
        db.session.delete(User.query.get(user_id))
    db.session.commit()
    return redirect(url_for('admin.show_user_list'))
