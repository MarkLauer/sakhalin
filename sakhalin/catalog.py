from flask import Blueprint, render_template
from flask_login import login_required

from .models import Item, Category

bp = Blueprint('catalog', __name__)


@bp.route('/')
@login_required
def index():
    new_items = Item.query.filter_by(status='new').all()
    special_items = Item.query.filter_by(status='special').all()
    return render_template(
        'catalog/index.html',
        new_items=new_items,
        special_items=special_items,
    )


@bp.route('/items', defaults={'category_id': None})
@bp.route('/items/<int:category_id>')
@login_required
def show_item_list(category_id):
    categories = Category.query.all()
    if category_id is None:
        category = categories[0]
    else:
        category = Category.query.get(category_id)
    items = category.items.all()
    return render_template(
        'catalog/item_list.html',
        items=items,
        categories=categories,
        category_id=category_id,
    )
